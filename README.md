# STM32 MIDI to Analog converter

A project built for STM32L476 Nucleo-64 which turn it into a MIDI to Analog converter.

## Usage

MIDI input is supposed to be connected to pin `D2`/`PA10`.

PITCH output comes out of pin `A2`/`PA4`.

GATE output comes out of pin `D4`/`PB5`.

MODULATION output comes out of pin `D13`/`PA5`.
