/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
#define CIRC_BUF_DEF(x, y)            \
  uint8_t x##_data_space[y];          \
  circ_buf_t x = {                    \
    .buffer = x##_data_space,         \
    .head = 0,                        \
    .tail = 0,                        \
    .maxlen = y                       \
  }

#define MID_PITCH_BEND_VALUE 8192
#define MAX_MODULATION_VALUE 127
#define OFF 0
#define ON 1
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct {
  uint8_t * const buffer;
  int head;
  int tail;
  const int maxlen;
} circ_buf_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DAC_HandleTypeDef hdac1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint8_t debug = 1;
uint8_t received;
CIRC_BUF_DEF(uart_midi_buffer, 16);

uint8_t current_note = OFF;
uint16_t current_pitch_bend = MID_PITCH_BEND_VALUE;

uint8_t midi_note_number_offset = 48;
uint8_t number_of_notes = 33;
uint16_t dor_values[33] = {
  124, // 0.1V
  248, // 0.2V
  372, // 0.3V
  496, // 0.4V
  620, // 0.5V
  745, // 0.6V
  869, // 0.7V
  993, // 0.8V
  1117, // 0.9V
  1241, // 1V
  1365, // 1.1V
  1482, // 1.2V

  1608, // 1.3V
  1730, // 1.4V
  1855, // 1.5V
  1980, // 1.6V
  2100, // 1.7V
  2220, // 1.8V
  2345, // 1.9V
  2470, // 2V
  2590, // 2.1V
  2710, // 2.2V
  2835, // 2.3V
  2960, // 2.4V

  3075, // 2.5V
  3205, // 2.6V
  3330, // 2.7V
  3450, // 2.8V
  3570, // 2.9V
  3690, // 3V
  3820, // 3.1V
  3945, // 3.2V
  4095, // 3.3V
};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_DAC1_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
int circ_buf_push(circ_buf_t *c, uint8_t data) {
  int next_pos;

  next_pos = c->head + 1;  // next is where head will point to after this write.
  if (next_pos >= c->maxlen)
    next_pos = 0;

  if (next_pos == c->tail)  // if the head + 1 == tail, circular buffer is full
    return -1;

  c->buffer[c->head] = data;  // Load data and then move
  c->head = next_pos;             // head to next data offset.
  return 0;  // return success to indicate successful push.
}

int circ_buf_pop(circ_buf_t *c, uint8_t *data) {
  int next_pos;

  if (c->head == c->tail)  // if the head == tail, we don't have any data
    return -1;

  next_pos = c->tail + 1;  // next is where tail will point to after this read.
  if(next_pos >= c->maxlen)
    next_pos = 0;

  *data = c->buffer[c->tail];  // Read data and then move
  c->tail = next_pos;              // tail to next offset.
  return 0;  // return success to indicate successful push.
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  circ_buf_push(&uart_midi_buffer, received);
  HAL_UART_Receive_IT(&huart2, &received, 1); // Start listening again for UART messages over USB
  HAL_UART_Receive_IT(&huart1, &received, 1); // Start listening again for UART messages over PIN D2/PA10
}

void set_debug_led(uint8_t state) {
  // if(debug == 0) return;

  // if(state == 1) HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
  // else HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
}

void print_uart(char message[], uint16_t byte) {
  if(debug == OFF) return;

  uint8_t message_to_print[50];
  uint16_t message_to_print_size = 0;

  message_to_print_size = sprintf(message_to_print, message, byte, byte, byte);
  HAL_UART_Transmit(&huart2, message_to_print, message_to_print_size, 1000); // Send data over UART
}

void print_uart_msg(char message[]) {
  if(debug == OFF) return;

  uint8_t message_to_print[50];
  uint16_t message_to_print_size = 0;

  message_to_print_size = sprintf(message_to_print, message);
  HAL_UART_Transmit(&huart2, message_to_print, message_to_print_size, 1000); // Send data over UART
}

uint16_t calculate_pitch_dor(uint8_t note_number, uint16_t pitch_bend) {
  float pitch_bend_percent;
  uint16_t result_dor = dor_values[note_number - midi_note_number_offset];

  // No pitch bend
  if(pitch_bend == MID_PITCH_BEND_VALUE)
    return result_dor;

  // Pitch bend up to +0.2V
  if(pitch_bend > MID_PITCH_BEND_VALUE) {
    pitch_bend_percent = (pitch_bend - MID_PITCH_BEND_VALUE + 1) / (float) MID_PITCH_BEND_VALUE;
                  // max +0.2V
    result_dor += (uint16_t) dor_values[1] * pitch_bend_percent;

  // Pitch bend down to -0.2V
  } else {
    pitch_bend_percent = abs(pitch_bend - MID_PITCH_BEND_VALUE) / (float) MID_PITCH_BEND_VALUE;
                  // max -0.2V
    result_dor -= (uint16_t) dor_values[1] * pitch_bend_percent;
  }

  return result_dor;
}

uint16_t calculate_current_pitch_dor() {
  return calculate_pitch_dor(current_note, current_pitch_bend);
}


uint16_t calculate_modulation_dor(uint8_t value) {
  float modulation_percent = value / (float) MAX_MODULATION_VALUE;
  uint16_t result_dor = (uint16_t) dor_values[number_of_notes - 1] * modulation_percent;
  return result_dor;
}

void set_gate() {
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
}

void unset_gate() {
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
}

void set_pitch(uint16_t dor_value) {
  print_uart("  DOR: 0x%x => %u \r\n", dor_value);
  DAC1->DHR12R1 = dor_value;
}

void set_modulation(uint16_t dor_value) {
  print_uart("  DOR: 0x%x => %u \r\n", dor_value);
  DAC1->DHR12R2 = dor_value;
}

void handle_note_on() {
  uint8_t midi_byte, note_byte, note_index;
  set_debug_led(ON);
  print_uart_msg("\r\nON:\r\n");

  circ_buf_pop(&uart_midi_buffer, &midi_byte);
  print_uart("  Note: 0x%x => %u \r\n", midi_byte);

  note_index = midi_byte - midi_note_number_offset;
  note_byte = midi_byte;

  circ_buf_pop(&uart_midi_buffer, &midi_byte);
  print_uart("  Velocity: 0x%x => %u \r\n", midi_byte);

  // Note ON with Velocity=0 => Note OFF
  if(midi_byte == 0 && current_note == note_byte) {
    set_pitch(OFF); // set PITCH to ~0V
    unset_gate(); // set GATE to ~0V
  } else if(note_index <= number_of_notes && note_byte >= midi_note_number_offset) {
    current_note = note_byte;
    set_pitch(calculate_current_pitch_dor()); // set PITCH to appropriate voltage
    set_gate(); // set GATE to 3.3V
  }
}

void handle_note_off() {
  uint8_t midi_byte, note_byte;
  set_debug_led(OFF);
  print_uart_msg("\r\nOFF:\r\n");

  circ_buf_pop(&uart_midi_buffer, &midi_byte);
  print_uart("  Note: 0x%x => %u \r\n", midi_byte);

  note_byte = midi_byte;

  circ_buf_pop(&uart_midi_buffer, &midi_byte);
  print_uart("  Velocity: 0x%x => %u \r\n", midi_byte);

  if(note_byte != current_note) return;

  set_pitch(OFF); // set PITCH to ~0V
  unset_gate(); // set GATE to ~0V
}

void handle_pitch_bend() {
  uint8_t msb, lsb;
  set_debug_led(ON);
  print_uart_msg("\r\nPITCH BEND:\r\n");
  circ_buf_pop(&uart_midi_buffer, &lsb);
  print_uart("  L: 0x%x => %u \r\n", lsb);

  circ_buf_pop(&uart_midi_buffer, &msb);
  print_uart("  M: 0x%x => %u \r\n", msb);
  current_pitch_bend = (msb << 7) + lsb;

  print_uart("  Pitch: 0x%x => %u \r\n", current_pitch_bend);
  set_pitch(calculate_current_pitch_dor()); // set PITCH to appropriate voltage
}

void handle_modulation() {
  uint8_t midi_byte;
  set_debug_led(ON);
  print_uart_msg("\r\nMODULATION:\r\n");
  circ_buf_pop(&uart_midi_buffer, &midi_byte); // controller number
  print_uart("  Number: 0x%x => %u \r\n", midi_byte);

  circ_buf_pop(&uart_midi_buffer, &midi_byte); // value
  print_uart("  Value: 0x%x => %u \r\n", midi_byte);

  set_modulation(calculate_modulation_dor(midi_byte));
}

void parse_midi() {
  uint8_t midi_byte;
  int byte_present = circ_buf_pop(&uart_midi_buffer, &midi_byte);
  if(byte_present == -1) return; // No bytes in the buffer

  switch (midi_byte >> 4) {
  case 0b1001: // Note ON
    handle_note_on();
    break;
  case 0b1000: // Note OFF
    handle_note_off();
    break;
  case 0b1110: // PITCH BEND
    handle_pitch_bend();
    break;
  case 0b1011: // MODULATION
    handle_modulation();
    break;
  }
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  set_pitch(OFF);
  set_gate(OFF);
  set_modulation(OFF);
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_DAC1_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart2, &received, 1);
  HAL_UART_Receive_IT(&huart1, &received, 1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  HAL_DAC_Start(&hdac1, DAC1_CHANNEL_1);
  HAL_DAC_Start(&hdac1, DAC1_CHANNEL_2);
  while(1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    parse_midi();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief DAC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC1_Init(void)
{

  /* USER CODE BEGIN DAC1_Init 0 */

  /* USER CODE END DAC1_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC1_Init 1 */

  /* USER CODE END DAC1_Init 1 */
  /** DAC Initialization
  */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config
  */
  sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_DISABLE;
  sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT2 config
  */
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC1_Init 2 */

  /* USER CODE END DAC1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 31250;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
